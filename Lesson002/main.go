package main

import "fmt"

func main() {
	// int operations
	fmt.Println("7+5=", 7+5)
	fmt.Println("7-5=", 7-5)
	fmt.Println("7*5=", 7*5)
	fmt.Println("7/5=", 7/5)
	fmt.Println("7%5=", 7%5)

	//task
	fmt.Println("32132 × 42452 = ", 32132*42452)

	// string operations
	fmt.Println(len("Hello World"))
	fmt.Println("Hello World"[1])
	fmt.Println("Hello " + "World")

	// bool operations
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(!true)

	// task
	fmt.Println((true && false) || (false && true) || !(false && false))
}
