# There are two ways to create a new variable. What kind?

1. Use keyword 'var': var x int =100
2. Use operation ':=': x:=10
