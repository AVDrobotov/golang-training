package main

import "fmt"

func main() {
	var x string = "Hello World"
	fmt.Println(x)

	var y string
	y = "Hello World"
	fmt.Println(y)

	var z string
	z = "first "
	fmt.Println(z)
	z = z + "second"
	fmt.Println(z)

	var x1 string = "hello"
	var y1 string = "world"
	fmt.Println(x1 == y1)

	var x2 string = "hello"
	var y2 string = "hello"
	fmt.Println(x2 == y2)

	x3 := "Hello World"
	fmt.Println(x3)

	var x4 = "Hello World"
	fmt.Println(x4)

	x5 := 5
	fmt.Println(x5)

	dogsName := "Max"
	fmt.Println("My dog's name is", dogsName)
	
}