# What is scope and how is the scope of a variable defined in Go?

1. The range of places where you are allowed to use variable is called the scope of the variable.
2. The variable exists within the nearest curly braces { } (a block) including any nested curly braces (blocks), but not outside of them.
