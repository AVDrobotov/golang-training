# How do you specify the direction of a channel type?

chan<- - send-only  channels
<-chan - receive-only channels
chan   - bi-directional channels.
