package main

import (
	"fmt"
	"time"
)

func Sleep(n time.Duration) {
	select {
	case <-time.After(n):
		//fmt.Println("End timeout")
	}
}

func main() {
	fmt.Println("Begin timeout",  time.Now().Unix())
	Sleep(50 * time.Second)
	fmt.Println("End sleep", time.Now().Unix())
	var input string
	fmt.Scanln(&input)
}
