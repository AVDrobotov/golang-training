# What are defer, panic and recover? How do you recover from a run-time panic?

1. Special statement called defer which schedules a function call to be run after the function completes.
2. Panic is a program crash function.
3. Recover is useful only when called inside deferred functions. Executing a call to recover inside a deferred function stops the panicking sequence by restoring normal execution and retrieves the error value passed to the call of panic.
