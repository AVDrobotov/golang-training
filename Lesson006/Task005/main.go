package main

import (
	"fmt"
)

func fib(n int) int {
	if n <= 0 {
		return 0
	}
	if n <= 2 {
		return 1
	}
	return fib(n-1) + fib(n-2)
}

var fibn = make(map[int64]int64)

func fib1(n int64) int64 {
	if val, ok := fibn[n]; ok {
		return val
	}
	if n <= 0 {
		fibn[n] = 0
		return 0
	}
	if n <= 2 {
		fibn[n] = 1
		return 1
	}
	fibn[n] = fib1(n-1) + fib1(n-2)
	return fibn[n]
}

func main() {
	for x := (int64) (0); x <= 100; x++ {
		fmt.Println(x, fib1(x))
	}
	// for x := 0; x <= 100; x++ {
	// 	fmt.Println(x, fib(x))
	// }
}
