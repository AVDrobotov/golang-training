# Golang Training lesson 6

Sixth application written in golang.
Theme is Returning Multiple Values.
For example:
func f() (int, int) {
  return 5, 6
}

func main() {
  x, y := f()
}
