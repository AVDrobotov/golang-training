package main

import "fmt"

func max(x ...int) (m int) {
	m = x[0]
	for _, v := range x {
		if m<v{m=v}
	  }
	return
}

func main() {
	fmt.Println(max(1,2,3))
}
