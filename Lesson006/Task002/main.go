package main

import "fmt"

func half(x int) (r int, e bool) {
	r = x / 2
	e = true
	if x%2 != 0 {
		e = false
	}
	return
}

func main() {
	fmt.Println(half(1))
	fmt.Println(half(2))
	fmt.Println(half(3))
}
