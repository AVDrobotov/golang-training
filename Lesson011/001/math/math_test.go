package math

import "testing"

func TestAverage(t *testing.T) {
	type args struct {
		xs []float64
	}
	type test struct {
		name string
		args args
		want float64
	}
	tests := []test{
		// TODO: Add test cases.
		{name: "first",
			args: args{xs: []float64{1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0}},
			want: 5.5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Average(tt.args.xs); got != tt.want {
				t.Errorf("Average() = %v, want %v", got, tt.want)
			}
		})
	}
}
