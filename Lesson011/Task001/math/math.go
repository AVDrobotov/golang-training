package math

// Average finds the average of a series of numbers
func Average(xs []float64) float64 {
	total := float64(0)
	if len(xs) > 0 {
		for _, x := range xs {
			total += x
		}
		return total / float64(len(xs))
	} else {
		return total
	}
}

func Max(xs []float64) float64 {
	if len(xs) == 0 {
		return 0
	} else {
		result := xs[0]
		for _, x := range xs {
			if result < x {
				result = x
			}
		}
		return result
	}
}

func Min(xs []float64) float64 {
	if len(xs) == 0 {
		return 0
	} else {
		result := xs[0]
		for _, x := range xs {
			if result > x {
				result = x
			}
		}
		return result
	}
}
