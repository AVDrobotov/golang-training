package main

import "fmt"
import "gitlab.com/AVDrobotov/golang-training/Lesson010/Task004/math"

func main() {
	xs := []float64{1, 2, 3, 4}
	avg := math.Average(xs)
	min := math.Min(xs)
	max := math.Max(xs)
	fmt.Println(avg, min, max)
}
