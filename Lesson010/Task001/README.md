# Why do we use packages?

Packages is used to 
- It reduces the chance of having overlapping names. This keeps our function names short and succinct.
- It organizes code so that its easier to find code you want to reuse.
- It speeds up the compiler by only requiring recompilation of smaller chunks of a program. Although we use the package fmt, we don't have to recompile it every time we change our program.
