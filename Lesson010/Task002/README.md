# What is the difference between an identifier that starts with a capital letter and one which doesn’t? (Average vs average)

Identifier that starts with a capital letter can be used outside the file.
Identifier that starts with a small letter can't be used outside the file.
