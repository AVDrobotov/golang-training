# Why would you use an embedded anonymous field instead of a normal named field?

Embedded anonymous fields are used to compose structures with an I-is relationship instead of an I-have.
