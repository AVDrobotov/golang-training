package main

import (
	"fmt"
	"math"
)

type Circle struct {
	x float64
	y float64
	r float64
}

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a*a + b*b)
}

func circleArea(c Circle) float64 {
	return math.Pi * c.r * c.r
}

func circleArea1(c *Circle) float64 {
	return math.Pi * c.r * c.r
}

func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}

type Person struct {
	Name string
}

func (p *Person) Talk() {
	fmt.Println("Hi, my name is", p.Name)
}

type Android struct {
	Person Person
	Model  string
}

type Android1 struct {
	Person
	Model string
}

type Shape interface {
	area() float64
}

func totalArea(shapes ...Shape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.area()
	}
	return area
}

type MultiShape struct {
	shapes []Shape
}

func (m *MultiShape) area() float64 {
	var area float64
	for _, s := range m.shapes {
		area += s.area()
	}
	return area
}

func main() {
	var c Circle
	c1 := new(Circle)
	c2 := Circle{x: 0, y: 0, r: 10}
	c3 := Circle{0, 0, 10}

	c.x = 10
	c.y = 5
	c.r = 10
	fmt.Println(c.x, c.y, c.r)
	fmt.Println(c1)
	fmt.Println(c2)
	fmt.Println(c3)

	c4 := Circle{0, 0, 5}
	fmt.Println(circleArea(c4))
	fmt.Println(circleArea1(&c4))
	fmt.Println(c4.area())

	r := Rectangle{0, 0, 10, 10}
	fmt.Println(r.area())

	a := new(Android)
	a.Person.Talk()

	a1 := new(Android1)
	a1.Talk()

	fmt.Println(totalArea(&c, &r))
}
