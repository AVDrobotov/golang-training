package main

import (
	"fmt"
	"math"
)

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a*a + b*b)
}

type Circle struct {
	x float64
	y float64
	r float64
}

func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r
}

func (c *Circle) perimeter() float64 {
	return 2.0 * math.Pi * c.r
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}

func (r *Rectangle) perimeter() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return 2*l + 2*w
}

// Shape - interface figures with area and perimeter
type Shape interface {
	area() float64
	perimeter() float64
}

func main() {
	var c Circle

	c.x = 10
	c.y = 5
	c.r = 10
	fmt.Println(c)
	r := Rectangle{0, 0, 10, 10}
	fmt.Println(r)

	fmt.Println(c.area())

	fmt.Println(r.area())


	fmt.Println(c.perimeter())

	fmt.Println(r.perimeter())
}
