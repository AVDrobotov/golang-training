# # How do you assign a value to a pointer?

When we write *xPtr = 0 we are saying “store the int 0 in the memory location xPtr refers to”.
